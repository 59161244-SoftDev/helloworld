/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sukit.helloworld;

import java.util.Scanner;

/**
 *
 * @author Xenon
 */
public class OXGame {
    
    /* Board */
    static void board(String[][] boardArr) {
        System.out.println("  1 2 3");
        System.out.printf("1 %s %s %s%n", boardArr[0][0], boardArr[0][1], boardArr[0][2]);
        System.out.printf("2 %s %s %s%n", boardArr[1][0], boardArr[1][1], boardArr[1][2]);
        System.out.printf("3 %s %s %s%n", boardArr[2][0], boardArr[2][1], boardArr[2][2]);
    }
    
    static String turnCycle(String turn) {
        if (turn.equals("X")) {
            return "O";
        } else {
            return "X";
        }
    }
    
    static boolean winCheck(String[][] arr) {
        /* Horizontal */
        String[] horizon = new String[3];
        horizon[0] = arr[0][0] + arr[0][1] + arr[0][2];
        horizon[1] = arr[1][0] + arr[1][1] + arr[1][2];
        horizon[2] = arr[2][0] + arr[2][1] + arr[2][2];
//      System.out.println("horizon: " + horizon[0] + " " + horizon[1] + " " + horizon[2]);

        /* Vertical */
        String[] vertical = new String[3];
        vertical[0] = arr[0][0] + arr[1][0] + arr[2][0];
        vertical[1] = arr[0][1] + arr[1][1] + arr[2][1];
        vertical[2] = arr[0][2] + arr[1][2] + arr[2][2];
//      System.out.println("vertical: " + vertical[0] + " " + vertical[1] + " " + vertical[2]);

        /* Diagonal */
        String[] diagonal = new String[2];
        diagonal[0] = arr[0][0] + arr[1][1] + arr[2][2];
        diagonal[1] = arr[0][2] + arr[1][1] + arr[2][0];
//      System.out.println("diagonal: " + diagonal[0] + " " + diagonal[1]);
        
        for(int i = 0; i < 3; i++){
            if(horizon[i].equals("XXX") || horizon[i].equals("OOO")){
                return false;
            } else if(vertical[i].equals("XXX") || vertical[i].equals("OOO")){
                return false;
            }
        }

        for(int i = 0; i < 2; i++){
            if(diagonal[i].equals("XXX") || diagonal[i].equals("OOO")){
                return false;
            }
        }
        return true;
    }
    
    public static void main(String[] args) {
        Scanner read = new Scanner(System.in);
        System.out.println("Welcome to OX Game");
        
        /* pre-Assign Var */
        String[][] boardArr = {{"-","-","-"},{"-","-","-"}, {"-","-","-"}};
        String turn = "X";
        board(boardArr);
        
        while(winCheck(boardArr)) {
            /* Input Row Col */
            System.out.printf("Turn %s%n", turn);
            boolean isFreeSlot;
            
            do{
                System.out.println("Please input Row Col:");
                int inRow = read.nextInt() -1;
                int inCol = read.nextInt() -1;
                if(!boardArr[inRow][inCol].equals("-")){
                    isFreeSlot = true;
                    System.out.println("Slot is not available!");
                } else {
                    isFreeSlot = false;
                    boardArr[inRow][inCol] = turn;
                }
            board(boardArr);
            }while(isFreeSlot);
            
            /* O-X Cycle*/
            turn = turnCycle(turn);
        }
        
        if(turn.equals("X")){
            System.out.println("player O Win!");
        } else {
            System.out.println("player X Win!");
        }
        System.out.println("Bye bye...");
        
        
       
        
        

        

        

        
    }
}
